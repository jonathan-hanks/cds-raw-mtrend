/*
cds_raw_mtrend
Copyright 2022  California Institute of Technology

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Neither the name of the California Institute of Technology (Caltech)
nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written
permission.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program, (in the COPYING-GPL-3 file in the main
project directory). If not, see http://www.gnu.org/licenses/
 */
#include <mtrend/raw_mtrend.hh>

#include <deque>
#include <iostream>
#include <string>

struct Options
{
    std::string filename{ };
    std::string err{ };
    bool        show_help{ false };
};

Options
parse_arguments( int argc, char* argv[] )
{
    Options                   opts{ };
    std::deque< std::string > args{ };
    for ( auto i = 1; i < argc; ++i )
    {
        args.emplace_back( argv[ i ] );
    }
    auto pop = [ &args ]( std::string option ) -> std::string {
        std::string s{ };
        if ( args.empty( ) )
        {
            std::string msg = option + " requires an argument";
            throw( std::runtime_error( msg ) );
        }
        s = args.front( );
        args.pop_front( );
        return s;
    };
    try
    {
        while ( !args.empty( ) )
        {
            std::string arg = args.front( );
            args.pop_front( );
            if ( arg == "-h" || arg == "--help" )
            {
                opts.show_help = true;
            }
            else if ( arg == "-f" || arg == "--file" )
            {
                opts.filename = pop( "-f/--file" );
            }
            else
            {
                throw std::runtime_error( arg + " is an unknown option" );
            }
        }
        if ( opts.filename.empty( ) )
        {
            throw std::runtime_error(
                "A file must be specified, please use -f|--file" );
        }
    }
    catch ( std::runtime_error& err )
    {
        opts.show_help = true;
        opts.err = err.what( );
    }
    return opts;
}

void
usage( const std::string prog, std::string& err_msg )
{
    if ( !err_msg.empty( ) )
    {
        std::cerr << err_msg << std::endl;
    }
    std::cout << "Usage:\n\t" << prog << " [options]\n";
    std::cout << "Options:\n";
    std::cout << "\t-h|--help\t\tThis help\n";
    std::cout << "\t-f|--file <file>\tInput file\n";
}

int
main( int argc, char* argv[] )
{
    auto opts = parse_arguments( argc, argv );
    if ( opts.show_help )
    {
        usage( argv[ 0 ], opts.err );
        return 1;
    }
    auto info = mtrend::characterize_mtrend_file( opts.filename );

    std::cout << opts.filename << "\n";
    std::cout << "start:   " << info.gps_start << "\n";
    std::cout << "end:     " << info.gps_end << "\n";
    std::cout << "samples: " << info.samples << "\n";

    return 0;
}
