add_executable(read_mtrend
        read_mtrend.cc)
target_link_libraries(read_mtrend PUBLIC
        raw_mtrend
        Boost::filesystem)