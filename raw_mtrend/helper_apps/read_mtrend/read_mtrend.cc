/*
cds_raw_mtrend
Copyright 2022  California Institute of Technology

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Neither the name of the California Institute of Technology (Caltech)
nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written
permission.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program, (in the COPYING-GPL-3 file in the main
project directory). If not, see http://www.gnu.org/licenses/
 */
#include <mtrend/raw_mtrend.hh>

#include <deque>
#include <ios>
#include <iostream>
#include <map>
#include <string>
#include <vector>

struct Options
{
    std::string                trend_name{ };
    mtrend::RawTrendType       type{ mtrend::RawTrendType::Mean };
    std::vector< std::string > directories{ };
    mtrend::gps_range          gps{ };
    std::string                err{ };
    bool                       show_help{ false };
};

static const std::map< std::string, mtrend::RawTrendType > trend_name_map{
    { "min_int", mtrend::RawTrendType::MinInt },
    { "min_float32", mtrend::RawTrendType::MinFloat32 },
    { "min_float64", mtrend::RawTrendType::MinFloat64 },
    { "max_int", mtrend::RawTrendType::MaxInt },
    { "max_float32", mtrend::RawTrendType::MaxFloat32 },
    { "max_float64", mtrend::RawTrendType::MaxFloat64 },
    { "n", mtrend::RawTrendType::N },
    { "rms", mtrend::RawTrendType::Rms },
    { "mean", mtrend::RawTrendType::Mean },
};

mtrend::RawTrendType
to_trend_type( const std::string& s )
{
    auto it = trend_name_map.find( s );
    if ( it == trend_name_map.end( ) )
    {
        throw std::runtime_error( s + " is an unknown trend type" );
    }
    return it->second;
}

unsigned int
to_uint( const std::string& s )
{
    return std::stoul( s );
}

Options
parse_arguments( int argc, char* argv[] )
{
    Options                   opts{ };
    std::deque< std::string > args{ };
    for ( auto i = 1; i < argc; ++i )
    {
        args.emplace_back( argv[ i ] );
    }
    auto pop = [ &args ]( const char* option_ ) -> std::string {
        std::string s{ };
        std::string option{ option_ ? option_ : "" };
        if ( args.empty( ) )
        {
            std::string msg = option + " requires an argument";
            throw( std::runtime_error( msg ) );
        }
        s = args.front( );
        args.pop_front( );
        return s;
    };
    try
    {
        while ( !args.empty( ) )
        {
            std::string arg = args.front( );
            args.pop_front( );
            if ( arg == "-h" || arg == "--help" )
            {
                opts.show_help = true;
            }
            else if ( arg == "-t" || arg == "--trend" )
            {
                opts.trend_name = pop( "-t/--trend" );
            }
            else if ( arg == "--type" )
            {
                opts.type = to_trend_type( pop( "--type" ) );
            }
            else if ( arg == "-s" )
            {
                opts.gps.start = to_uint( pop( "-s" ) );
            }
            else if ( arg == "-e" )
            {
                opts.gps.end = to_uint( pop( "-e" ) );
            }
            else if ( arg == "-d" )
            {
                opts.directories.emplace_back( pop( "-d" ) );
            }
            else
            {
                throw std::runtime_error( arg + " is an unknown option" );
            }
        }
        if ( opts.trend_name.empty( ) )
        {
            throw std::runtime_error(
                "A trend name must be specified, please use -t|--trend" );
        }
    }
    catch ( std::runtime_error& err )
    {
        opts.show_help = true;
        opts.err = err.what( );
    }
    return opts;
}

void
usage( const std::string prog, std::string& err_msg )
{
    if ( !err_msg.empty( ) )
    {
        std::cerr << err_msg << std::endl;
    }
    std::cout << "Usage:\n\t" << prog << " [options]\n";
    std::cout << "Options:\n";
    std::cout << "\t-h|--help\t\tThis help\n";
    std::cout << "\t-t|--trend <name>\tTrend name\n";
    std::cout << "\t--type <name>\tTrend type [mean]\n";
    std::cout << "-s <gps>\tGPS start time\n";
    std::cout << "-e <gps>\tGPS end time\n";
    std::cout << "-d <dir>\traw minute trend directory (may be specified more "
                 "than once\n";
    std::cout << "\nTrend types: \n";
    for ( const auto tt : trend_name_map )
    {
        std::cout << "\t" << tt.first << "\n";
    }
}

int
main( int argc, char* argv[] )
{
    auto opts = parse_arguments( argc, argv );
    if ( opts.show_help )
    {
        usage( argv[ 0 ], opts.err );
        return 1;
    }

    auto req_size = mtrend::required_buffer_size( opts.gps, opts.type );
    std::vector< char > buf{ };
    buf.resize( req_size );
    mtrend::read( opts.trend_name,
                  opts.directories,
                  opts.gps,
                  opts.type,
                  mtrend::DestinationBuffer( buf ) );
    for ( auto i = 0; i < 10 && i < buf.size( ); ++i )
    {
        printf( "%x ", static_cast< unsigned char >( buf[ i ] ) );
    }
    printf( "\n" );
    return 0;
}