/*
cds_raw_mtrend
Copyright 2022  California Institute of Technology

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Neither the name of the California Institute of Technology (Caltech)
nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written
permission.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program, (in the COPYING-GPL-3 file in the main
project directory). If not, see http://www.gnu.org/licenses/
 */
#include <mtrend/raw_mtrend.hh>
#include "private/crc8.hh"
#include "private/search.hh"
#include "private/file_handle.hh"

#include <algorithm>
#include <cstddef>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/endian/conversion.hpp>

namespace
{
    boost::endian::order
    to_boost( mtrend::byte_order byte_order )
    {
        switch ( byte_order )
        {
        case mtrend::byte_order::BIG:
            return boost::endian::order::big;
        case mtrend::byte_order::LITTLE:
            return boost::endian::order::little;
        default:
            return boost::endian::order::native;
        }
    }

    template < typename T >
    void
    flip( mtrend::DestinationBuffer& buffer )
    {
        char*       start = reinterpret_cast< char* >( buffer.data );
        const char* end = start + buffer.len;
        for ( auto cur = start; cur < end; cur += sizeof( T ) )
        {
            *reinterpret_cast< T* >( cur ) =
                boost::endian::endian_reverse( *reinterpret_cast< T* >( cur ) );
        }
    }
} // namespace

namespace mtrend
{
    constexpr std::size_t
              field_offset( RawTrendType type )
    {
        switch ( type )
        {
        case RawTrendType::MinInt:
            return offsetof( raw_trend_record_struct, tb.min.I );
        case RawTrendType::MinFloat32:
            return offsetof( raw_trend_record_struct, tb.min.F );
        case RawTrendType::MinFloat64:
            return offsetof( raw_trend_record_struct, tb.min.D );
        case RawTrendType::MaxInt:
            return offsetof( raw_trend_record_struct, tb.max.I );
        case RawTrendType::MaxFloat32:
            return offsetof( raw_trend_record_struct, tb.max.F );
        case RawTrendType::MaxFloat64:
            return offsetof( raw_trend_record_struct, tb.max.D );
        case RawTrendType::N:
            return offsetof( raw_trend_record_struct, tb.n );
        case RawTrendType::Rms:
            return offsetof( raw_trend_record_struct, tb.rms );
        case RawTrendType::Mean:
            return offsetof( raw_trend_record_struct, tb.mean );
        }
        return 0;
    }
    static_assert( field_offset( RawTrendType::MinInt ) == 4,
                   "Min int must be 4 bytes in" );
    static_assert( field_offset( RawTrendType::MinFloat32 ) == 4,
                   "Min float32 must be 4 bytes in" );
    static_assert( field_offset( RawTrendType::MinFloat64 ) == 4,
                   "Min float64 must be 4 bytes in" );
    static_assert( field_offset( RawTrendType::MaxInt ) == 12,
                   "Max int must be 12 bytes in" );
    static_assert( field_offset( RawTrendType::MaxFloat32 ) == 12,
                   "Max float32 must be 12 bytes in" );
    static_assert( field_offset( RawTrendType::MaxFloat64 ) == 12,
                   "Max float64 must be 12 bytes in" );
    static_assert( field_offset( RawTrendType::N ) == 20,
                   "N must be 20 bytes in" );
    static_assert( field_offset( RawTrendType::Rms ) == 24,
                   "Rms must be 24 bytes in" );
    static_assert( field_offset( RawTrendType::Mean ) == 32,
                   "Mean must be 32 bytes in" );

    constexpr std::size_t
              field_size( RawTrendType type )
    {
        switch ( type )
        {
        case RawTrendType::MinInt:
        case RawTrendType::MinFloat32:
        case RawTrendType::MaxInt:
        case RawTrendType::MaxFloat32:
        case RawTrendType::N:
            return 4;
        case RawTrendType::MinFloat64:
        case RawTrendType::MaxFloat64:
        case RawTrendType::Rms:
        case RawTrendType::Mean:
            return 8;
        }
        return 0;
    }
    static_assert( field_size( RawTrendType::MinInt ) == 4,
                   "Trend ints are 4 bytes" );
    static_assert( field_size( RawTrendType::MinFloat32 ) == 4,
                   "Trend floats are 4 bytes" );
    static_assert( field_size( RawTrendType::MinFloat64 ) == 8,
                   "Trend doubles are 8 bytes" );
    static_assert( field_size( RawTrendType::MaxInt ) == 4,
                   "Trend ints are 4 bytes" );
    static_assert( field_size( RawTrendType::MaxFloat32 ) == 4,
                   "Trend floats are 4 bytes" );
    static_assert( field_size( RawTrendType::MaxFloat64 ) == 8,
                   "Trend doubles are 8 bytes" );
    static_assert( field_size( RawTrendType::N ) == 4,
                   "Trend ints are 4 bytes" );
    static_assert( field_size( RawTrendType::Rms ) == 8,
                   "Trend doubles are 8 bytes" );
    static_assert( field_size( RawTrendType::Mean ) == 8,
                   "Trend doubles are 8 bytes" );

    void
    byte_swap( DestinationBuffer& buf,
               byte_order         target,
               RawTrendType       data_type )
    {
        auto order = to_boost( target );
        if ( order != boost::endian::order::native )
        {
            switch ( mtrend::field_size( data_type ) )
            {
            case 8:
                flip< std::uint64_t >( buf );
                break;
            case 4:
                flip< std::uint32_t >( buf );
                break;
            }
        }
    }

    mtrend_characterization
    characterize_mtrend( file_handle& f )
    {
        mtrend_characterization mtrend_range{ };
        try
        {
            auto         finfo = f.stat( );
            unsigned int blocks =
                finfo.st_size / sizeof( raw_trend_record_struct );
            if ( blocks == 0 )
            {
                return mtrend_range;
            }
            mtrend_range.samples = blocks;
            raw_trend_record_struct sample{ };

            f.read( &sample, 1 );
            mtrend_range.gps_start = sample.gps;

            f.seek( sizeof( raw_trend_record_struct ) * ( blocks - 1 ),
                    SEEK_SET );
            f.read( &sample, 1 );
            mtrend_range.gps_end = sample.gps + 60;
        }
        catch ( ... )
        {
            mtrend_characterization empty{ };
            mtrend_range = empty;
        }
        return mtrend_range;
    }

    mtrend_characterization
    characterize_mtrend_file( const std::string& fname )
    {
        mtrend_characterization mtrend_range{ };
        try
        {
            file_handle f( fname );
            return characterize_mtrend( f );
        }
        catch ( ... )
        {
            mtrend_characterization empty{ };
            mtrend_range = empty;
        }
        return mtrend_range;
    }

    class file_gps_accessor
    {
    public:
        explicit file_gps_accessor( file_handle& f ) : f_{ f }
        {
        }
        unsigned int
        operator[]( int index )
        {
            unsigned int gps{ 0 };

            f_.seek( index * sizeof( raw_trend_record_struct ), SEEK_SET );
            f_.read( &gps, 1 );
            return gps;
        }

    private:
        file_handle& f_;
    };

    constexpr int
    gps_stride( ) noexcept
    {
        return 60;
    }

    std::size_t
    required_buffer_size( gps_range gps, RawTrendType type )
    {
        auto samples = ( gps.end - gps.start ) / gps_stride( );
        return samples * field_size( type );
    }

    std::string
    to_str( const RawTrendType type )
    {
        switch ( type )
        {
        case RawTrendType::Mean:
            return "mean";
        case RawTrendType::Rms:
            return "rms";
        case RawTrendType::N:
            return "n";
        case RawTrendType::MinInt:
            return "min_int";
        case RawTrendType::MinFloat32:
            return "min_float32";
        case RawTrendType::MinFloat64:
            return "min_float64";
        case RawTrendType::MaxInt:
            return "max_int";
        case RawTrendType::MaxFloat32:
            return "max_float32";
        case RawTrendType::MaxFloat64:
            return "max_float64";
        default:
            return "unknown";
        }
    }

    void
    read( const std::string&                trend_name,
          const std::vector< std::string >& directories,
          gps_range                         gps,
          RawTrendType                      type,
          DestinationBuffer                 dest )
    {
        char* out = dest.data;

        if ( gps.start % gps_stride( ) != 0 || gps.end % gps_stride( ) != 0 )
        {
            throw unaligned_access( );
        }
        if ( gps.end < gps.start )
        {
            throw invalid_gps_range( );
        }
        if ( gps.start == gps.end )
        {
            return;
        }

        auto target_offset = field_offset( type );
        auto target_size = field_size( type );

        auto samples = ( gps.end - gps.start ) / gps_stride( );
        if ( !dest.data || ( dest.len != samples * target_size ) )
        {
            throw invalid_buffer_size( );
        }

        const std::string hash_name{ detail::crc8_str( trend_name ) };

        auto cur_gps = gps.start;
        for ( const auto& dir : directories )
        {
            boost::filesystem::path file_path =
                boost::filesystem::path{ dir } / hash_name / trend_name;
            file_handle f{ file_path.string( ), no_except{} };
            if ( !f )
            {
                continue;
            }

            auto trend_info = characterize_mtrend( f );
            if ( trend_info.gps_end <= cur_gps )
            {
                continue;
            }

            file_gps_accessor gps_access{ f };
            auto              cur_range =
                detail::find_range( gps_access,
                                    detail::range{ 0, trend_info.samples },
                                    detail::range{ cur_gps, gps.end } );
            auto cur_count = cur_range.stop - cur_range.start;
            if ( cur_count == 0 )
            {
                continue;
            }
            f.seek( cur_range.start * sizeof( raw_trend_record_struct ),
                    SEEK_SET );
            while ( cur_count )
            {
                raw_trend_record_struct cur_record{ };
                f.read( &cur_record, 1 );

                if ( cur_record.gps > cur_gps )
                {
                    auto pad_count{ ( cur_record.gps - cur_gps ) /
                                    gps_stride( ) };
                    auto pad_size{ pad_count * target_size };
                    std::fill( out, out + ( pad_size ), 0 );
                    out += pad_size;
                }
                std::memcpy( out,
                             reinterpret_cast< char* >( &cur_record ) +
                                 target_offset,
                             target_size );
                out += target_size;

                cur_gps = cur_record.gps + gps_stride( );

                --cur_count;
            }
            if ( cur_gps == gps.end )
            {
                break;
            }
        }
        if ( cur_gps < gps.end )
        {
            auto pad_count{ ( gps.end - cur_gps ) / gps_stride( ) };
            auto pad_size{ pad_count * target_size };
            std::fill( out, out + ( pad_size ), 0 );
        }
    }
} // namespace mtrend