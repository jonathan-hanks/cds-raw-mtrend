#include "catch.hpp"

#include "private/crc8.hh"

TEST_CASE( "Test crc8_str" )
{
    REQUIRE( mtrend::detail::crc8_str( "H1:FEC-56_CPU_METER_MAX" ) == "1b" );
    REQUIRE( mtrend::detail::crc8_str( "H1:HPI-HAM3_BLRMS_Y_100M_300M" ) ==
             "2f" );
    REQUIRE( mtrend::detail::crc8_str( "H1:TCS-SIM_IFO_G_YARM_LIMIT" ) == "0" );
    REQUIRE( mtrend::detail::crc8_str(
                 "H1:SYS-TIMING_C_MA_A_PORT_4_NODE_UPLINKCRCERRCOUNT" ) ==
             "c" );
}