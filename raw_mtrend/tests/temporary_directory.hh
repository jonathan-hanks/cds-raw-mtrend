//
// Created by jonathan.hanks on 11/10/22.
//

#ifndef CDS_RAW_MTREND_TEMPORARY_DIRECTORY_HH
#define CDS_RAW_MTREND_TEMPORARY_DIRECTORY_HH

#include <boost/filesystem.hpp>
#include <sys/stat.h>

class TemporaryDirectory
{
public:
    TemporaryDirectory( )
    {
        auto root = boost::filesystem::temp_directory_path( );
        int  rc = 0;
        do
        {
            auto name = boost::filesystem::unique_path( );
            path_ = root / name;
            rc = ::mkdir( path_.string( ).c_str( ), 0700 );
        } while ( rc == EEXIST );
        if ( rc != 0 )
        {
            throw std::runtime_error( "Unable to create temporary directory" );
        }
    }
    TemporaryDirectory( const TemporaryDirectory& ) = delete;
    TemporaryDirectory( TemporaryDirectory&& ) = delete;
    TemporaryDirectory& operator=( const TemporaryDirectory& ) = delete;
    TemporaryDirectory& operator=( TemporaryDirectory&& ) = delete;
    ~TemporaryDirectory( )
    {
        try
        {
            boost::filesystem::remove_all( path_ );
        }
        catch ( ... )
        {
        }
    }
    const boost::filesystem::path&
    path( ) const noexcept
    {
        return path_;
    }

private:
    boost::filesystem::path path_;
};

#endif // CDS_RAW_MTREND_TEMPORARY_DIRECTORY_HH
