//
// Created by jonathan.hanks on 11/14/22.
//
#include "private/search.hh"
#include "catch.hpp"

#include <limits>
#include <ostream>

namespace
{

    template < typename T >
    class VectorAccessor
    {
    public:
        explicit VectorAccessor( const std::vector< T >& data ) : data_{ data }
        {
        }
        VectorAccessor( const VectorAccessor& ) = delete;
        VectorAccessor( VectorAccessor&& ) = delete;

        T
        operator[]( int index ) const
        {
            return data_[ index ];
        }

    private:
        const std::vector< T >& data_;
    };

} // namespace

TEST_CASE( "Midpoint" )
{
    struct TestCase
    {
        unsigned int a;
        unsigned int b;
        unsigned int expected;
    };
    std::vector< TestCase > test_cases{
        { 0, 0, 0 },
        { 0, 1, 0 },
        { 0, 2, 1 },
        { 0, 3, 1 },
        { 0, 4, 2 },
        { 0, 5, 2 },
        { 0, 6, 3 },
        { 0, 99, 49 },
        { 0, 100, 50 },
        { 1, 101, 51 },
        { 0,
          std::numeric_limits< unsigned int >::max( ),
          std::numeric_limits< unsigned int >::max( ) / 2 }
    };
    for ( const auto& test_case : test_cases )
    {
        unsigned int actual =
            mtrend::detail::midpoint( test_case.a, test_case.b );
        REQUIRE( actual == test_case.expected );
        REQUIRE( actual >= test_case.a );
        REQUIRE( actual <= test_case.b );
    }
}

TEST_CASE( "Test find_lower" )
{
    struct TestCase
    {
        std::vector< int > sequence;
        unsigned int       start;
        unsigned int       stop;
        int                search_for;
        unsigned int       expected;
    };
    std::vector< TestCase > test_cases{
        {
            { 3, 5 },
            0,
            2,
            5,
            1,
        },
        {
            { 5, 7 },
            0,
            2,
            5,
            0,
        },
        {
            { 5 },
            0,
            1,
            5,
            0,
        },
        {
            { 5 },
            0,
            0,
            5,
            0,
        },
        {
            { },
            0,
            0,
            5,
            0,
        },
        {
            { 1, 2, 3, 4, 5 },
            4,
            5,
            5,
            4,
        },
        {
            { 1, 2, 3, 4, 5 },
            4,
            5,
            1,
            4,
        },
        {
            { 1, 2, 3, 4, 5 },
            4,
            5,
            6,
            5,
        },
        {
            { 1, 2, 3, 4, 5 },
            5,
            5,
            6,
            5,
        },
        {
            { 1, 2, 3, 4, 5 },
            0,
            0,
            6,
            0,
        },
        {
            { 1, 2, 3, 4, 5 },
            0,
            5,
            0,
            0,
        },
        {
            { 1, 2, 3, 4, 5 },
            0,
            5,
            6,
            5,
        },
        {
            { 1, 2, 3, 4, 5 },
            0,
            5,
            3,
            2,
        },
        {
            { -1, 0, 1, 2, 3, 4, 5 },
            1,
            5,
            3,
            4,
        },
        {
            { -1, 0, 1, 2, 3, 4, 5 },
            1,
            5,
            5,
            5,
        },
        {
            { -1, 0, 1, 2, 3, 4, 5 },
            1,
            5,
            -1,
            1,
        },
    };
    for ( const auto& test_case : test_cases )
    {
        VectorAccessor< int > accessor( test_case.sequence );
        unsigned int          actual =
            mtrend::detail::find_lower( accessor,
                                        test_case.start,
                                        test_case.stop,
                                        [ &test_case ]( int entry ) -> bool {
                                            return entry < test_case.search_for;
                                        } );
        REQUIRE( actual == test_case.expected );
    }
}

namespace mtrend
{
    namespace detail
    {
        std::ostream&
        operator<<( std::ostream& os, const range& r )
        {
            os << "(" << r.start << ", " << r.stop << ")";
            return os;
        }
    } // namespace detail
} // namespace mtrend

TEST_CASE( "find_range finds a range in a sequence" )
{
    struct TestCase
    {
        std::vector< int >    sequence;
        mtrend::detail::range bounds;
        mtrend::detail::range search_for;
        mtrend::detail::range expected;
    };
    std::vector< TestCase > test_cases{
        {
            { },
            { 0, 0 },
            { 5, 600 },
            { 0, 0 },
        },
        {
            { 10 },
            { 0, 1 },
            { 5, 600 },
            { 0, 1 },
        },
        {
            { 10, 11, 12, 23, 24, 35, 60 },
            { 0, 7 },
            { 70, 600 },
            { 7, 7 },
        },
        {
            { 10, 11, 12, 23, 24, 35, 60 },
            { 0, 7 },
            { 10, 60 },
            { 0, 7 },
        },
        {
            { 10, 11, 12, 23, 24, 35, 60 },
            { 0, 7 },
            { 0, 12 },
            { 0, 3 },
        },
        {
            { 10, 11, 12, 23, 24, 35, 60 },
            { 0, 7 },
            { 0, 5 },
            { 0, 0 },
        },
        {
            { 0, 1, 2, 3, 4, 5, 6 },
            { 0, 7 },
            { 2, 4 },
            { 2, 5 },
        },
        {
            { 0, 1, 2, 3, 4, 5, 6 },
            { 0, 7 },
            { 2, 7 },
            { 2, 7 },
        },
    };
    for ( const auto& test_case : test_cases )
    {
        VectorAccessor< int > accessor( test_case.sequence );
        const auto            actual = mtrend::detail::find_range(
            accessor, test_case.bounds, test_case.search_for );
        REQUIRE( actual == test_case.expected );
    }
}