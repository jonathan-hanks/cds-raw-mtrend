#include "catch.hpp"

#include <mtrend/raw_mtrend.hh>
#include "private/crc8.hh"
#include "private/file_handle.hh"

#include <boost/filesystem.hpp>

#include "temporary_directory.hh"

#include <algorithm>
#include <chrono>
#include <iterator>
#include <iostream>
#include <set>

using no_except = mtrend::no_except;
using file_handle = mtrend::file_handle;

void
add_extra( file_handle& f, int extra )
{
    if ( extra > 0 )
    {
        std::vector< char > data{ };
        data.resize( extra );
        for ( auto i = 0; i < extra; ++i )
        {
            data[ i ] = static_cast< char >( i % 128 );
        }
        f.write( data.data( ), extra );
    }
}

std::string
create_empty_trend1( const boost::filesystem::path& dir )
{
    std::string         fpath = ( dir / "trend1" ).string( );
    mtrend::file_handle f( fpath, "wb" );
    return fpath;
}

std::string
create_single_trend2( const boost::filesystem::path& dir, int extra = 0 )
{
    std::string                     fpath = ( dir / "trend2" ).string( );
    mtrend::file_handle             f( fpath, "wb" );
    mtrend::raw_trend_record_struct sample{ };
    sample.gps = 1000000020;
    sample.tb.max.D = 3.1415 + 2;
    sample.tb.min.D = -3.1415 + 2;
    sample.tb.mean = 2.0;
    sample.tb.rms = 1.0;
    sample.tb.n = 2048;
    f.write( &sample, 1 );
    add_extra( f, extra );
    return fpath;
}

std::string
create_multi_trend3( const boost::filesystem::path& dir, int extra = 0 )
{
    std::string         fpath = ( dir / "trend2" ).string( );
    mtrend::file_handle f( fpath, "wb" );

    unsigned int start = 1000000020;

    for ( int i = 0; i < 60; ++i )
    {
        mtrend::raw_trend_record_struct sample{ };
        sample.gps = start + ( i * 60 );
        sample.tb.max.D = 3.1415 + 2;
        sample.tb.min.D = -3.1415 + 2;
        sample.tb.mean = 2.0;
        sample.tb.rms = 1.0;
        sample.tb.n = 2048;
        if ( i < 40 || i >= 50 )
        {
            f.write( &sample, 1 );
        }
    }
    add_extra( f, extra );
    return fpath;
}

std::string
create_trend_from_indexes( const boost::filesystem::path& dir,
                           const std::string              chan_name,
                           const std::vector< int >&      indexes )
{
    std::string         fpath = ( dir / chan_name ).string( );
    mtrend::file_handle f( fpath, "wb" );

    if ( indexes.empty( ) )
    {
        return fpath;
    }
    unsigned int start = 1000000020;

    for ( const auto& i : indexes )
    {
        mtrend::raw_trend_record_struct sample{ };
        sample.gps = start + ( i * 60 );
        sample.tb.max.D = 3.1415 + 2;
        sample.tb.min.D = -3.1415 + 2;
        sample.tb.mean = 2.0;
        sample.tb.rms = 1.0;
        sample.tb.n = 2048;
        f.write( &sample, 1 );
    }
    return fpath;
}

std::vector< std::string >
create_full_read_test1( TemporaryDirectory& tdir )
{
    std::vector< std::string > output_dirs{ };
    std::string                chan_name = "CHAN1";
    std::string                dir_hash = mtrend::detail::crc8_str( chan_name );
    const boost::filesystem::path& root_dir = tdir.path( );

    auto sub_dir = [ root_dir ]( const char* dir ) -> boost::filesystem::path {
        return ( root_dir / dir );
    };

    struct test_dir
    {
        boost::filesystem::path p;
        std::vector< int >      indexes;
    };
    std::vector< test_dir > paths = {
        { sub_dir( "main" ), { 0, 1, 2, 3, 4 } },
        { sub_dir( "archive1" ), {} },
        { sub_dir( "archive2" ), { 20, 21, 22, 28, 29 } },
        { sub_dir( "archive3" ), { 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 } }
    };
    for ( const auto& td : paths )
    {
        auto dir = td.p / dir_hash;
        boost::filesystem::create_directories( dir );
        if ( !td.indexes.empty( ) )
        {
            create_trend_from_indexes( dir, chan_name, td.indexes );
        }
    }

    std::transform(
        paths.begin( ),
        paths.end( ),
        std::back_inserter( output_dirs ),
        []( const test_dir& td ) -> std::string { return td.p.string( ); } );
    return output_dirs;
}

TEST_CASE( "characterization of a missing file should not be an exception" )
{
    TemporaryDirectory tdir{ };
    std::string        fname = ( tdir.path( ) / "non-existing" ).string( );
    mtrend::mtrend_characterization range =
        mtrend::characterize_mtrend_file( fname );
    REQUIRE( range.samples == 0 );
    REQUIRE( range.gps_start == 0 );
    REQUIRE( range.gps_end == 0 );
}

TEST_CASE( "test characterization of a file" )
{
    TemporaryDirectory tdir{ };

    auto                            fname = create_empty_trend1( tdir.path( ) );
    mtrend::mtrend_characterization range =
        mtrend::characterize_mtrend_file( fname );
    REQUIRE( range.samples == 0 );
    REQUIRE( range.gps_start == 0 );
    REQUIRE( range.gps_end == 0 );
}

TEST_CASE( "test characterization of a single sample file" )
{
    TemporaryDirectory tdir{ };

    auto fname = create_single_trend2( tdir.path( ) );
    mtrend::mtrend_characterization range =
        mtrend::characterize_mtrend_file( fname );
    REQUIRE( range.samples == 1 );
    REQUIRE( range.gps_start == 1000000020 );
    REQUIRE( range.gps_end == 1000000080 );
}

TEST_CASE( "test characterization of a single sample file - with extra bytes" )
{
    TemporaryDirectory tdir{ };

    auto fname = create_single_trend2( tdir.path( ), 39 );
    mtrend::mtrend_characterization range =
        mtrend::characterize_mtrend_file( fname );
    REQUIRE( range.samples == 1 );
    REQUIRE( range.gps_start == 1000000020 );
    REQUIRE( range.gps_end == 1000000080 );
}

TEST_CASE( "test characterization of a multi sample file" )
{
    TemporaryDirectory tdir{ };

    auto                            fname = create_multi_trend3( tdir.path( ) );
    mtrend::mtrend_characterization range =
        mtrend::characterize_mtrend_file( fname );
    REQUIRE( range.samples == 50 );
    REQUIRE( range.gps_start == 1000000020 );
    REQUIRE( range.gps_end == 1000003620 );
}

TEST_CASE( "test characterization of a multi sample file - with extra bytes" )
{
    TemporaryDirectory tdir{ };

    auto fname = create_multi_trend3( tdir.path( ), 3 );
    mtrend::mtrend_characterization range =
        mtrend::characterize_mtrend_file( fname );
    REQUIRE( range.samples == 50 );
    REQUIRE( range.gps_start == 1000000020 );
    REQUIRE( range.gps_end == 1000003620 );
}

TEST_CASE( "read calls with invalid buffer sizes should fail" )
{
    const int samples = 100;
    struct TestCase
    {
        mtrend::RawTrendType type;
        std::size_t          size;
        bool                 is_null{ false };
    };
    std::vector< TestCase > test_cases{
        { mtrend::RawTrendType::Mean, 0 },
        { mtrend::RawTrendType::N, 0 },
        { mtrend::RawTrendType::Mean, 99 },
        { mtrend::RawTrendType::N, 99 },
        { mtrend::RawTrendType::Mean, 101 },
        { mtrend::RawTrendType::N, 101 },
        { mtrend::RawTrendType::Mean, 0, true },
        { mtrend::RawTrendType::N, 0, true },
        { mtrend::RawTrendType::Mean, 99, true },
        { mtrend::RawTrendType::N, 99, true },
        { mtrend::RawTrendType::Mean, 101, true },
        { mtrend::RawTrendType::N, 101, true },
        { mtrend::RawTrendType::Mean, 100, true },
        { mtrend::RawTrendType::N, 100, true },
    };

    TemporaryDirectory tdir{ };

    std::vector< double > dest{ };
    dest.resize( 1024 );

    auto dirs = create_full_read_test1( tdir );
    for ( const auto& test_case : test_cases )
    {
        try
        {
            auto multiplier =
                ( test_case.type == mtrend::RawTrendType::Mean ? 8 : 4 );
            mtrend::read(
                "CHAN1",
                dirs,
                mtrend::gps_range{ 1000000020, 1000000020 + ( 60 * samples ) },
                test_case.type,
                mtrend::DestinationBuffer(
                    reinterpret_cast< char* >(
                        ( test_case.is_null ? nullptr : dest.data( ) ) ),
                    test_case.size * multiplier ) );
            FAIL( "mtrend::read should have failed with an invalid buffer size "
                  "error" );
        }
        catch ( mtrend::invalid_buffer_size& err )
        {
        }
    }
}

TEST_CASE( "read calls valid times, end >= start and aligned to 60s gps" )
{
    struct TestCase
    {
        mtrend::gps_range gps;
    };
    std::vector< TestCase > test_cases{
        { mtrend::gps_range{ 1000000000, 1000000020 } },
        { mtrend::gps_range{ 1000000020, 1000000090 } },
        { mtrend::gps_range{ 1000000000, 1000000100 } },
    };

    TemporaryDirectory tdir{ };

    auto dirs = create_full_read_test1( tdir );
    for ( const auto& test_case : test_cases )
    {
        try
        {
            std::vector< double > dest{ };
            dest.resize( test_case.gps.end - test_case.gps.start );
            mtrend::read( "CHAN1",
                          dirs,
                          test_case.gps,
                          mtrend::RawTrendType::Mean,
                          mtrend::DestinationBuffer( dest ) );
            FAIL( "mtrend::read should have failed with an unaligned access "
                  "error" );
        }
        catch ( mtrend::unaligned_access& err )
        {
        }
    }
    try
    {
        std::vector< double > dest{ };
        dest.resize( 1 );
        mtrend::read( "CHAN1",
                      dirs,
                      mtrend::gps_range{ 1000000020, 1000000020 - 60 },
                      mtrend::RawTrendType::Mean,
                      mtrend::DestinationBuffer( dest ) );
        FAIL(
            "mtrend::read should have failed with an invalid_gps_range error" );
    }
    catch ( mtrend::invalid_gps_range& err )
    {
    }
}

TEST_CASE( "test read" )
{
    TemporaryDirectory tdir{ };

    auto dirs = create_full_read_test1( tdir );

    std::vector< double > dest{ };
    dest.resize( 40 );
    mtrend::read( "CHAN1",
                  dirs,
                  mtrend::gps_range{ 1000000020, 1000000020 + ( 40 * 60 ) },
                  mtrend::RawTrendType::Mean,
                  mtrend::DestinationBuffer( dest ) );
    std::set< int > valid_indices = { 0,  1,  2,  3,  4,  20, 21, 22, 28, 29,
                                      30, 31, 32, 33, 34, 35, 36, 37, 38, 39 };
    for ( auto i = 0; i < dest.size( ); i++ )
    {
        if ( valid_indices.find( i ) != valid_indices.end( ) )
        {
            REQUIRE( dest.at( i ) == 2.0 );
        }
        else
        {
            REQUIRE( dest.at( i ) == 0.0 );
        }
    }
}