/*
cds_raw_mtrend
Copyright 2022  California Institute of Technology

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Neither the name of the California Institute of Technology (Caltech)
nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written
permission.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program, (in the COPYING-GPL-3 file in the main
project directory). If not, see http://www.gnu.org/licenses/
 */
#include "private/crc8.hh"

#include <cstdio>

namespace mtrend
{

    namespace detail
    {
        const int crc8_Table[] = {
            0,   94,  188, 226, 97,  63,  221, 131, 194, 156, 126, 32,  163,
            253, 31,  65,  157, 195, 33,  127, 252, 162, 64,  30,  95,  1,
            227, 189, 62,  96,  130, 220, 35,  125, 159, 193, 66,  28,  254,
            160, 225, 191, 93,  3,   128, 222, 60,  98,  190, 224, 2,   92,
            223, 129, 99,  61,  124, 34,  192, 158, 29,  67,  161, 255, 70,
            24,  250, 164, 39,  121, 155, 197, 132, 218, 56,  102, 229, 187,
            89,  7,   219, 133, 103, 57,  186, 228, 6,   88,  25,  71,  165,
            251, 120, 38,  196, 154, 101, 59,  217, 135, 4,   90,  184, 230,
            167, 249, 27,  69,  198, 152, 122, 36,  248, 166, 68,  26,  153,
            199, 37,  123, 58,  100, 134, 216, 91,  5,   231, 185, 140, 210,
            48,  110, 237, 179, 81,  15,  78,  16,  242, 172, 47,  113, 147,
            205, 17,  79,  173, 243, 112, 46,  204, 146, 211, 141, 111, 49,
            178, 236, 14,  80,  175, 241, 19,  77,  206, 144, 114, 44,  109,
            51,  209, 143, 12,  82,  176, 238, 50,  108, 142, 208, 83,  13,
            239, 177, 240, 174, 76,  18,  145, 207, 45,  115, 202, 148, 118,
            40,  171, 245, 23,  73,  8,   86,  180, 234, 105, 55,  213, 139,
            87,  9,   235, 181, 54,  104, 138, 212, 149, 203, 41,  119, 244,
            170, 72,  22,  233, 183, 85,  11,  136, 214, 52,  106, 43,  117,
            151, 201, 74,  20,  246, 168, 116, 42,  200, 150, 21,  75,  169,
            247, 182, 232, 10,  84,  215, 137, 107, 53
        };

        unsigned int
        crc8( const char* s )
        {
            int crc = 0;
            for ( ; *s; s++ )
            {
                crc = crc8_Table[ crc ^ *s ];
            }
            return crc;
        }

        std::string
        crc8_str( const std::string& s )
        {
            char buf[ 3 ];
            std::sprintf( buf, "%x", crc8( s.c_str( ) ) );
            return buf;
        }
    } // namespace detail
} // namespace mtrend