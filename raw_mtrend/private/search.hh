/*
cds_raw_mtrend
Copyright 2022  California Institute of Technology

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Neither the name of the California Institute of Technology (Caltech)
nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written
permission.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program, (in the COPYING-GPL-3 file in the main
project directory). If not, see http://www.gnu.org/licenses/
 */
#ifndef CDS_RAW_MTREND_SEARCH_HH
#define CDS_RAW_MTREND_SEARCH_HH

namespace mtrend
{
    namespace detail
    {
        struct range
        {
            unsigned int start;
            unsigned int stop;

            bool
            operator==( const range& other ) const noexcept
            {
                return start == other.start && stop == other.stop;
            }
        };

        static inline unsigned int
        midpoint( unsigned int a, unsigned int b )
        {
            return a + ( b - a ) / 2;
        }

        // find the "left most" index, ie last index with a gps <= gps
        template < typename Accessor, typename Predicate >
        unsigned int
        find_lower( Accessor&    accessor,
                    unsigned int start,
                    unsigned int end,
                    Predicate    p )
        {
            auto lower{ start };
            auto upper{ end };
            while ( lower < upper )
            {
                if ( lower + 1 == upper )
                {
                    auto element = accessor[ lower ];
                    if ( p( element ) )
                    {
                        return upper;
                    }
                    else
                    {
                        return lower;
                    }
                }
                auto mid = detail::midpoint( lower, upper );
                auto element = accessor[ mid ];
                auto pred_val = p( element );
                if ( pred_val )
                {
                    lower = mid;
                }
                else
                {
                    upper = mid;
                }
            }
            return end;
        }

        template < typename Accessor >
        range
        find_range( Accessor& a, range bounds, range search_for )
        {
            range results{ };

            results.start =
                find_lower( a,
                            bounds.start,
                            bounds.stop,
                            [ search_for ]( unsigned int entry ) -> bool {
                                return entry < search_for.start;
                            } );
            results.stop =
                find_lower( a,
                            bounds.start,
                            bounds.stop,
                            [ search_for ]( unsigned int entry ) -> bool {
                                return entry <= search_for.stop;
                            } );
            return results;
        }

    } // namespace detail
} // namespace mtrend

#endif // CDS_RAW_MTREND_SEARCH_HH
