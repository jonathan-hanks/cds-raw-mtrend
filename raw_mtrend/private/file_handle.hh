/*
cds_raw_mtrend
Copyright 2022  California Institute of Technology

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Neither the name of the California Institute of Technology (Caltech)
nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written
permission.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program, (in the COPYING-GPL-3 file in the main
project directory). If not, see http://www.gnu.org/licenses/
 */

#ifndef CDS_RAW_MTREND_FILE_HANDLE_HH
#define CDS_RAW_MTREND_FILE_HANDLE_HH

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cstdio>
#include <stdexcept>
#include <string>

namespace mtrend
{
    struct no_except
    {
    };

    class file_handle
    {
    public:
        file_handle( ) = default;
        explicit file_handle( const std::string& fname )
            : f_{ std::fopen( fname.c_str( ), "rb" ) }
        {
            if ( !f_ )
            {
                throw std::runtime_error( "Unable to open file" );
            }
        }
        file_handle( const std::string& fname, no_except )
            : f_{ std::fopen( fname.c_str( ), "rb" ) }
        {
        }
        explicit file_handle( const std::string& fname, const char* mode )
            : f_{ std::fopen( fname.c_str( ), mode ) }
        {
            if ( !f_ )
            {
                throw std::runtime_error( "Unable to open file" );
            }
        }
        file_handle( const std::string& fname, const char* mode, no_except )
            : f_{ std::fopen( fname.c_str( ), mode ) }
        {
        }
        explicit file_handle( FILE* f ) noexcept : f_{ f }
        {
        }
        file_handle( const file_handle& ) = delete;
        file_handle( file_handle&& other ) noexcept : f_{ other.release( ) }
        {
        }
        ~file_handle( )
        {
            close( );
        }

        file_handle& operator=( const file_handle& ) = delete;
        file_handle&
        operator=( file_handle&& other ) noexcept
        {
            if ( f_ != other.f_ )
            {
                close( );
                f_ = other.release( );
            }
            return *this;
        }

        file_handle&
        operator=( FILE* f ) noexcept
        {
            if ( f_ != f )
            {
                close( );
                f_ = f;
            }
            return *this;
        }

        operator bool( ) const noexcept
        {
            return f_ != nullptr;
        }

        FILE*
        get( ) const noexcept
        {
            return f_;
        }

        FILE*
        release( ) noexcept
        {
            FILE* f = f_;
            f_ = nullptr;
            return f;
        }

        void
        close( ) noexcept
        {
            if ( f_ )
            {
                std::fclose( f_ );
                f_ = nullptr;
            }
        }

        void
        seek( long offset, int whence )
        {
            validate( );
            if ( std::fseek( f_, offset, whence ) != 0 )
            {
                throw std::runtime_error( "Unable to seek" );
            }
        }

        int
        seek( long offset, int whence, no_except )
        {
            return std::fseek( f_, offset, whence );
        }

        template < typename T >
        void
        read( T* dest, size_t count )
        {
            validate( dest );
            if ( std::fread(
                     static_cast< void* >( dest ), sizeof( T ), count, f_ ) !=
                 count )
            {
                throw std::runtime_error(
                    "unable to read all requested entries" );
            }
        }

        template < typename T >
        int
        read( T* dest, size_t count, no_except ) noexcept
        {
            return std::fread(
                static_cast< void* >( dest ), sizeof( T ), count, f_ );
        }

        template < typename T >
        void
        write( T* src, size_t count )
        {
            validate( src );
            if ( std::fwrite(
                     static_cast< void* >( src ), sizeof( T ), count, f_ ) !=
                 count )
            {
                throw std::runtime_error(
                    "unable to write all requested entries" );
            }
        }

        template < typename T >
        int
        write( T* src, size_t count, no_except ) noexcept
        {
            return std::fwrite(
                static_cast< void* >( src ), sizeof( T ), count, f_ );
        }

        bool
        eof( )
        {
            return ( f_ ? feof( f_ ) != 0 : true );
        }

        bool
        error( )
        {
            return ( f_ ? ferror( f_ ) != 0 : true );
        }

        struct stat
        stat( )
        {
            struct stat finfo
            {
            };
            validate( );
            if ( fstat( fileno( f_ ), &finfo ) != 0 )
            {
                throw std::runtime_error( "unable to stat file" );
            }
            return finfo;
        }

        int
        stat( struct stat* finfo, no_except ) noexcept
        {
            return fstat( fileno( f_ ), finfo );
        }

    private:
        void
        validate( )
        {
            if ( !f_ )
            {
                throw std::runtime_error( "file_handle not initialized" );
            }
        }

        template < typename T >
        void
        validate( T* ptr )
        {
            if ( !f_ )
            {
                throw std::runtime_error( "file_handle not initialized" );
            }
            if ( !ptr )
            {
                throw std::runtime_error( "invalid pointer passed" );
            }
        }

        FILE* f_{ nullptr };
    };

} // namespace mtrend

#endif // CDS_RAW_MTREND_FILE_HANDLE_HH
