/*
cds_raw_mtrend
Copyright 2022  California Institute of Technology

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Neither the name of the California Institute of Technology (Caltech)
nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written
permission.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program, (in the COPYING-GPL-3 file in the main
project directory). If not, see http://www.gnu.org/licenses/
 */
#ifndef CDS_RAW_MTREND_HH
#define CDS_RAW_MTREND_HH

#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>

namespace mtrend
{
    class unaligned_access : public std::runtime_error
    {
    public:
        unaligned_access( )
            : std::runtime_error( "MTrend access where the start/stop time was "
                                  "not divisible by 60" )
        {
        }
        ~unaligned_access( ) override = default;
    };

    class invalid_gps_range : public std::runtime_error
    {
    public:
        invalid_gps_range( )
            : std::runtime_error( "MTrend access where start > end" )
        {
        }
        ~invalid_gps_range( ) override = default;
    };

    class invalid_buffer_size : public std::runtime_error
    {
    public:
        invalid_buffer_size( )
            : std::runtime_error(
                  "MTrend destination buffer is the wrong size" )
        {
        }
        ~invalid_buffer_size( ) override = default;
    };

    enum class RawTrendType
    {
        MinInt,
        MinFloat32,
        MinFloat64,
        MaxInt,
        MaxFloat32,
        MaxFloat64,
        N,
        Rms,
        Mean,
    };

    enum class byte_order
    {
        BIG,
        LITTLE,
        NATIVE,
    };

    struct DestinationBuffer
    {
        char*  data{ nullptr };
        size_t len{ 0 };

        DestinationBuffer( char* backing_store, size_t size )
            : data{ backing_store }, len{ size }
        {
        }
        template < typename T >
        explicit DestinationBuffer( std::vector< T >& backing_store )
            : data{ reinterpret_cast< char* >( backing_store.data( ) ) }, len{
                  backing_store.size( ) * sizeof( T )
              }
        {
            static_assert(
                std::is_integral< T >::value ||
                    std::is_floating_point< T >::value,
                "Quick creation of a DestinationBuffer requires std::vector of"
                "numeric or floating point values" );
        }
        DestinationBuffer( const DestinationBuffer& ) = default;
    };

    /// Convert a Destination buffer from native byte order to a specified byte
    /// order
    extern void byte_swap( DestinationBuffer& buffer,
                           byte_order         target,
                           RawTrendType       data_type );

    /// Raw minute trend data substructure.
    typedef struct
    {
        union
        {
            int    I;
            double D;
            float  F;
        } min;
        union
        {
            int    I;
            double D;
            float  F;
        } max;
        int n; // the number of valid points used in calculating min, max, rms
               // and mean
        double rms;
        double mean;
    } __attribute__( ( packed ) ) trend_block_t;

    /// Raw minute trend data file structure.
    typedef struct raw_trend_record_struct
    {
        unsigned int  gps;
        trend_block_t tb;
    } raw_trend_record_struct;

    static_assert( sizeof( int ) == 4, "4 byte ints please" );
    static_assert( sizeof( unsigned int ) == 4, "4 byte unsigned ints please" );
    static_assert( sizeof( mtrend::trend_block_t ) == 36,
                   "trend_block_t must be 36 bytes" );
    static_assert( sizeof( mtrend::raw_trend_record_struct ) == 40,
                   "The raw_trend_record_struct must be 40 bytes" );

    struct mtrend_characterization
    {
        unsigned int gps_start{ 0 };
        unsigned int gps_end{ 0 };
        unsigned int samples{ 0 };
    };

    struct gps_range
    {
        unsigned int start{ 0 };
        unsigned int end{ 0 };
    };

    extern mtrend_characterization
    characterize_mtrend_file( const std::string& fname );

    std::size_t required_buffer_size( gps_range gps, RawTrendType type );

    void read( const std::string&                trend_name,
               const std::vector< std::string >& directories,
               gps_range                         gps,
               RawTrendType                      type,
               DestinationBuffer                 dest );
} // namespace mtrend
#endif // CDS_RAW_MTREND_HH