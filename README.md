# cds_raw_mtrend

A library used to read from CDS raw minute trend files.

## Background

As an optimization the CDS group writes out 1 file for each
channel recorded in frames, this channel is the raw trend file.
It contains 5 trends (min, max, mean, rms, n) for the channel,
calculated over a minute.

These files are optimized for reading large amounts of data
from a single channel, very quickly.

## Compiling

This requires a C++ compiler, cmake, boost (it makes use of
the filesystem and endian modules), pybind11, python3, numpy,
and catch2 (for tests).

Make a build directory, move into it, call cmake, and them build.
<pre>
cd build

cmake ..

make 
make install
</pre>

## Using

### C++ Example

<pre>
#include &lt;string&gt;
#include &lt;vector&gt;
#include &lt;mtrend/raw_mtrend.h&gt;

int main(int argc, char* argv[])
{
    std::string channel = "H0:VAC-LY_Y1_PT120A_PRESS_TORR";
    std::vector&lt;char&gt; buffer{};

    mtrend::gps_range timespan(1332831600, 1335337200);

    buffer.resize(mtrend::required_buffer_size(timespan, mtrend::RawTrendType::Mean));

    std::vector&lt;std::string&gt; dirs = {
        "/raw-trends/current",
        "/raw-trends/0',
        "/raw-trends/1',
        "/raw-trends/2",
    };

    mtrend::read( channel, dirs, timespan, mtrend::RawTrendType::mean, mtrend::DestinationBuffer(buffer));
    // at this point the data is in buffer

    return 0;
}
</pre>

## Python example

<pre>
import raw_trend
import numpy as np

channel = "H0:VAC-LY_Y1_PT120A_PRESS_TORR"

timespan = raw_trend.gps_range( 1332831600, 1335337200 )
buf = np.zeros(raw_trend.required_buffer_size(timespan, raw_trend.RawTrendType.Mean))

dirs = [
    "/raw-trends/current",
    "/raw-trends/0",
    "/raw-trends/1",
    "/raw-trends/2",
]
raw_trend.read(channel, dirs, timespan, raw_trend.RawTrendType.Mean, buf) 
# the data is now in buf
</pre>

# Notes

 * Missing data is filled in with zeros.  
   * This includes invalid channels.
 * GPS Start/Stop times must be divisible by 60.
 * The raw trends do not store type information, so there are variations on
some of the trend types (min & max have int32, float32, float64 variants).
It is the responsibility of the calling program to determine the right
field/type to request.

# License
This is licensed under the GPL-3 or later see COPYING-GPL-3

This software uses (and distributes a copy of) the catch2 library which is
licensed under the boost 1.0 license.


