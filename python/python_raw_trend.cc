//
// Created by jonathan.hanks on 12/1/22.
//
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#include <mtrend/raw_mtrend.hh>

namespace py = pybind11;

namespace
{
    void
    mtrend_read(
        const std::string&                trend_name,
        const std::vector< std::string >& directories,
        mtrend::gps_range                 gps,
        mtrend::RawTrendType              type,
        py::array_t< char, py::array::c_style | py::array::forcecast >& dest,
        mtrend::byte_order byte_order )
    {
        if ( dest.ndim( ) != 1 )
        {
            throw std::runtime_error(
                "Expecting a one dimensional destination array" );
        }
        auto                   buf_info = dest.request( );
        py::gil_scoped_release release;
        auto start = reinterpret_cast< char* >( buf_info.ptr );
        auto end = start + buf_info.size;
        auto buffer = mtrend::DestinationBuffer( start, buf_info.size );
        mtrend::read( trend_name, directories, gps, type, buffer );
        mtrend::byte_swap( buffer, byte_order, type );
    }
} // namespace

PYBIND11_MODULE( raw_trend, m )
{
    m.doc( ) = "Read LIGO CDS raw minute trend files";

    py::enum_< mtrend::byte_order >(
        m, "byte_order", "Specify the byte order for the output" )
        .value( "BIG", mtrend::byte_order::BIG )
        .value( "LITTLE", mtrend::byte_order::LITTLE )
        .value( "NATIVE", mtrend::byte_order::NATIVE );

    py::enum_< mtrend::RawTrendType >(
        m, "RawTrendType", "Specify the trend to retrieve" )
        .value( "MinInt", mtrend::RawTrendType::MinInt )
        .value( "MinFloat32", mtrend::RawTrendType::MinFloat32 )
        .value( "MinFloat64", mtrend::RawTrendType::MinFloat64 )
        .value( "MaxInt", mtrend::RawTrendType::MaxInt )
        .value( "MaxFloat32", mtrend::RawTrendType::MaxFloat32 )
        .value( "MaxFloat64", mtrend::RawTrendType::MaxFloat64 )
        .value( "N", mtrend::RawTrendType::N )
        .value( "Rms", mtrend::RawTrendType::Rms )
        .value( "Mean", mtrend::RawTrendType::Mean );

    py::class_< mtrend::gps_range >( m, "gps_range", "Specify a gps start/end" )
        .def( py::init( []( unsigned int start,
                            unsigned int stop ) -> mtrend::gps_range {
                  return mtrend::gps_range{ start, stop };
              } ),
              "Create a gps range [start, stop)" )
        .def_readwrite( "start", &mtrend::gps_range::start )
        .def_readwrite( "end", &mtrend::gps_range::end );

    m.def( "required_buffer_size",
           &mtrend::required_buffer_size,
           "Determine the buffer size to use for a read request" );

    m.def( "read",
           &mtrend_read,
           py::arg( "trend_name" ),
           py::arg( "directories" ),
           py::arg( "gps" ),
           py::arg( "type" ),
           py::arg( "dest" ),
           py::arg( "byte_order" ) = mtrend::byte_order::NATIVE,
           "Read from a minute trend" );
}