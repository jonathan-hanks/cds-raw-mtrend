#!/bin/bash

SOURCE_DIRS="raw_mtrend python"
CFORMAT=`which clang-format`

for dir in ${SOURCE_DIRS}; do
    echo "Reformatting in ${dir}"
    find ${dir} -type f -iregex ".*\.\(c\|cc\|cxx\|cpp\|h\|hh\|hpp\|hxx\)" \! -iname catch.hpp \! -exec "${CFORMAT}" -i -style=file -fallback-style=none {} \;
done
